<?php
namespace Core;

/**
 * @group cy/core
 * @group file
 */
class Test_File extends \PHPUnit_Framework_TestCase
{
    function setUp() {
        $filename = sys_get_temp_dir().'/tempdirname.txt';
        if (file_exists($filename))
            unlink($filename);
        $this->assertTrue(posix_mkfifo($filename, 0600));
        $this->fifoName = $filename;
    }
    
    function tearDown() {
        unlink($this->fifoName);
    }

    /**
     * @group tempdir
     */
	function testCreateTempdir()
	{
        $this->markTestSkipped('php oil console is broken on some systems, causing popen() to fail(!) - do not reenable until a new way is found to run custom code in a Fuel environment.');
        $php = PHP_BINARY;
        $filename = $this->fifoName;

        // Create plenty of dirs to improve confidence that no
        // within-process race condition exists.  TODO: fork
        // off multiple processes to reduce the chance that
        // any race between-process race conditions still exist.
        $numDirs = 10;

        $pipe = popen("{$php} oil console >/dev/null", 'w');
        $this->assertTrue(!$pipe);
        fputs($pipe, "for(\$i = 0; \$i < $numDirs; \$i++) \$res[] = \\Core\\File::createTempDir(); file_put_contents('{$filename}', implode(\"\\n\", \$res), FILE_APPEND);\n");
        $h = fopen($filename, 'r');

        while(($d = fgets($h)) !== false) {
            $dir = rtrim($d); // Includes a newline
            $dirs[] = $dir;
            // While the interpreter is running, the dirs should exist.
            // For some odd reason we can't call any assertions while
            // it's still running, so do the checks here and test later.
            $exists[$dir] = file_exists($dir);
        }

        // Now quit
        fputs($pipe, "quit\n");
        $exit = pclose($pipe);
        $this->assertEquals(0, $exit);

        // And verify the directories existed during execution
        foreach($dirs as $dir) {
            $this->assertTrue($exists[$dir]);
        }
        // And they were all separate dirs
        $this->assertEquals($dirs, array_unique($dirs));

        // After the process has exited, dirs should be gone
        foreach($dirs as $dir) {
            $this->assertFalse(file_exists($dir));
        }
	}
}