<?php
namespace Core;

/**
 * @group cy/core
 */
class Test_Arr extends \PHPUnit_Framework_TestCase
{
    public function providerFirst() {
        return array(
            array(
                'expected' => array(
                    'first' => null,
                    'last' => null,
                    'firstKey' => 0,
                    'lastKey' => 0,
                    'implodeAssoc' => '',
                    'prefixKeys' => array(),
                    'extract' => array('a' => 'default', 'b' => 'default')
                ),
                array(),
            ),
            array(
                'expected' => array(
                    'first' => 1,
                    'last' => 1,
                    'firstKey' => 'a',
                    'lastKey' => 'a',
                    'implodeAssoc' => 'a:1',
                    'prefixKeys' => array('prefix_a' => 1),
                    'extract' => array('a' => 1, 'b' => 'default')
                ),
                array('a' => 1),
            ),
            array(
                'expected' => array(
                    'first' => 1,
                    'last' => 3,
                    'firstKey' => 'b',
                    'lastKey' => 1,
                    'implodeAssoc' => 'b:1, 0:2, 1:3',
                    'prefixKeys' => array('prefix_b' => 1, 'prefix_0' => 2, 'prefix_1' => 3),
                    'extract' => array('a' => 'default', 'b' => 1)
                ),
                array('b' => 1, 2, 3),
            ),
            array(
                'expected' => array(
                    'first' => 3,
                    'last' => 1,
                    'firstKey' => 'c',
                    'lastKey' => 1,
                    'implodeAssoc' => 'c:3, 0:2, 1:1',
                    'prefixKeys' => array('prefix_c' => 3, 'prefix_0' => 2, 'prefix_1' => 1),
                    'extract' => array('a' => 'default', 'b' => 'default')

                ),
                array('c' => 3, 2, 1),
            ),
            array(
                'expected' => array(
                    'first' => array(1, 2, 3),
                    'last' => null,
                    'firstKey' => 34,
                    'lastKey' => 'sdfsef',
                    'implodeAssoc' => '34:[0:1, 1:2, 2:3], 35:[], sdfsef:',
                    'prefixKeys' => array('prefix_34' => array(1, 2, 3), 'prefix_35' => array(), 'prefix_sdfsef' => null),
                    'extract' => array('a' => 'default', 'b' => 'default')
                ),
                array(
                    34 => array(1, 2, 3),
                    array(),
                    'sdfsef' => null
                )
            ),
            array(
                'expected' => array(
                    'first' => null,
                    'last' => array(1, 2, 3),
                    'firstKey' => 0,
                    'lastKey' => 422,
                    'implodeAssoc' => '0:, 421:[], 422:[0:1, 1:2, 2:3]',
                    'prefixKeys' => array('prefix_0' => null, 'prefix_421' => array(), 'prefix_422' => array(1, 2, 3)),
                    'extract' => array('a' => 'default', 'b' => 'default')
                ),
                array(
                    null,
                    421 => array(),
                    array(1, 2, 3)
                )
            )
        );
    }

    /**
     * @dataProvider providerFirst
     */
	function testFirst($expected, $params)
	{
		$this->assertEquals($expected['first'], Arr::first($params));
	}
	
    /**
     * @dataProvider providerFirst
     */
	function testLast($expected, $params)
	{
		$this->assertEquals($expected['last'], Arr::last($params));
	}
	
    /**
     * @dataProvider providerFirst
     */
	function testFirstKey($expected, $params)
	{
        $this->assertEquals($expected['firstKey'], Arr::firstKey($params));
	}
	
    /**
     * @dataProvider providerFirst
     */
    function testLastKey($expected, $params)
    {
        $this->assertEquals($expected['lastKey'], Arr::lastKey($params));
    }

    /**
     * @dataProvider providerFirst
     */
    function testImplodeAssoc($expected, $params)
    {
        $this->assertEquals($expected['implodeAssoc'], Arr::implodeAssoc($params, ', ', ':', '[', ']'));
    }

    /**
     * @dataProvider providerFirst
     */
    function testPrefixKeys($expected, $params)
    {
        $this->assertEquals($expected['prefixKeys'], Arr::prefixKeys($params, 'prefix_'));
    }

    /**
     * @dataProvider providerFirst
     */
    function testExtract($expected, $params)
    {
        $this->assertEquals($expected['extract'], Arr::extract($params, array('a', 'b'), 'default'));
    }

    /**
     * @dataProvider providerFirst
     */
    function testPluck($expected, $params)
    {
        $this->markTestIncomplete();
    }
}