<?php
namespace Core;

/**
 * @group cy/core
 */
class Test_Date extends \PHPUnit_Framework_TestCase
{
    public function providerWeekNumbersInMonth()
    {
        return [[
                2014,
                1,
                [1, 2, 3, 4]
            ], [
                2014,
                8,
                [31, 32, 33, 34, 35]
            ], [
                2014,
                9,
                [36, 37, 38, 39]
        ]];
    }

    /**
     * @dataProvider providerWeekNumbersInMonth
     */
    function testWeekNumbersInMonth($year, $month, $expected)
    {
        $this->assertEquals($expected, Date::week_numbers_in_month($year, $month));
    }
}