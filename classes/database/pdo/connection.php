<?php
namespace Core;

/**
 * Add custom attributes to pdo.
 * 
 * @author AB Zainuddin
 *
 */
class Database_PDO_Connection extends \Fuel\Core\Database_PDO_Connection
{
    protected $isSetCustomAttributes = false;
    
    public function connect()
    {
        parent::connect();
        
        // Set ATTR_EMULATE_PREPARES to retrieve values in native mysql type.
        if (!$this->isSetCustomAttributes && array_key_exists('attributes', $this->_config))
		{
            foreach ($this->_config['attributes'] as $key => $val) {
                $this->_connection->setAttribute($key, $val);
            }
            
            $this->isSetCustomAttributes = true;
        }
    }
    
    public function setConfig(array $config) {
        $this->_config = $config;
    }
}