<?php
namespace Core;

/**
 * Changed _find_* methodes.
 *
 */
class Migrate extends \Fuel\Core\Migrate
{
    /**
     * @var array   current migrations registered in the database
     */
    public static $migrations = array();

    /**
     * Always overwrite config file with database
     * migrations table.
     */
    public static function _init()
    {
        parent::_init();

        \Config::set('migrations.version', static::$migrations);
    }

    /**
     * Original does not check the always_load.
     *
     * @param string name of the module
     *
     * @return array
     */
    protected static function _find_vendor($name = null)
    {
        $files = array();

        if ($name)
        {
            // find a vendor package
            $files = glob(VENDORPATH . '*/' .$name.'/'.\Config::get('migrations.folder').'*_*.php');
            
        }
        else
        {
            // find all modules
            $files = array_merge($files, glob(VENDORPATH.'*/'.\Config::get('migrations.folder').'*_*.php'));
            
        }        

        return $files;
    }

    public static function write_revert($name, $type, $file)
    {
        return parent::write_revert($name, $type, $file);
    }

    public static function write_install($name, $type, $file)
    {
        return parent::write_install($name, $type, $file);   
    }
}
