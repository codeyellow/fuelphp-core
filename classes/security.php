<?php
namespace Core;

class Security extends \Fuel\Core\Security
{

    /**
     * Filename Security
     * 
     * @link codeigniter
     * @param string
     * @param bool
     * @return string
     */
    public static function sanitizeFilename($str, $relative_path = FALSE)
    {
        $bad = array(
            "../",
            "<!--",
            "-->",
            "<",
            ">",
            "'",
            '"',
            '&',
            '$',
            '#',
            '{',
            '}',
            '[',
            ']',
            '=',
            ';',
            '?',
            "%20",
            "%22",
            "%3c", // <
            "%253c", // <
            "%3e", // >
            "%0e", // >
            "%28", // (
            "%29", // )
            "%2528", // (
            "%26", // &
            "%24", // $
            "%3f", // ?
            "%3b", // ;
            "%3d"  // =
        );
        
        if (!$relative_path) {
            $bad[] = './';
            $bad[] = '/';
        }
        
        $str = static::removeInvisibleCharacters($str, FALSE);
        return stripslashes(str_replace($bad, '', $str));
    }
    
    /**
     * Remove Invisible Characters
     *
     * This prevents sandwiching null characters
     * between ascii characters, like Java\0script.
     *
     * @link codeignter
     * @access	public
     * @param	string
     * @return	string
     */
    public static function removeInvisibleCharacters($str, $url_encoded = TRUE)
    {
        $non_displayables = array();
    
        // every control character except newline (dec 10)
        // carriage return (dec 13), and horizontal tab (dec 09)
    
        if ($url_encoded)
        {
            $non_displayables[] = '/%0[0-8bcef]/';	// url encoded 00-08, 11, 12, 14, 15
            $non_displayables[] = '/%1[0-9a-f]/';	// url encoded 16-31
        }
    
        $non_displayables[] = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S';	// 00-08, 11, 12, 14-31, 127
    
        do
        {
            $str = preg_replace($non_displayables, '', $str, -1, $count);
        }
        while ($count);
    
        return $str;
    }
}