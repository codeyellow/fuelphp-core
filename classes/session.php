<?php
namespace Core;

class Session extends \Fuel\Core\Session
{
    /**
	 * Add expire_on_close.
	 *
	 * @param	void
	 * @access	public
	 * @return	Session_Driver object
	 */
    public static function instance($instance = null)
    {
        $instance = parent::instance($instance);
        
        $instance->set_config('expire_on_close', $instance->get('expire_on_close', true));
        
        return $instance;
    }
}


