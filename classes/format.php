<?php
namespace Core;

class Format extends \Fuel\Core\Format
{
    protected function _from_xml($string,$recursive=false)
    {
        is_string($string) && $string = trim($string);
        return parent::_from_xml($string,$recursive);
    }
}
