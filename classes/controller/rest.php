<?php
namespace Core;

class Controller_Rest extends \Fuel\Core\Controller_Rest
{
    /**
     * Don't send 204 No Content on empty data.
     * 
     * @see Fuel\Core\Controller_Rest
     */
    protected function response($data = array(), $http_status = null)
    {
        if ((is_array($data) and empty($data)) or ($data == ''))
        {
            $http_status === null ? $this->response->status = $this->no_data_status : $this->response->status = $http_status;
            return $this->response;
        }
        
        return parent::response($data, $http_status);
    }
}