<?php
namespace Core;

class Debug extends \Fuel\Core\Debug
{
    static function d()
    {
        echo '<pre>';
        
        foreach(func_get_args() as $arg){
            print_r($arg);
        }
        
        echo '</pre>';
        die();
    }

    static function firstKey(array $array)
    {
        return current(array_keys($array));
    }

    public static function in_arrayi($needle, $haystack)
    {
        return in_arrayi($needle, $haystack);
    }
}

// Alias for \Debug::d();
if (!function_exists('dd'))
{
    function dd()
    {
        return call_user_func_array('\Debug::d', func_get_args());
    }
}