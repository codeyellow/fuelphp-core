<?php
namespace Core;

class View extends \Fuel\Core\View
{
	static function forgeTemplate($file = null, $data = null, $auto_filter = null)
	{
		$data === null ? $data = array('view' => $file) : $data['view'] = $file;
		return parent::forge('template/page', $data, $auto_filter);
	}
	
	static function firstKey(array $array)
	{
		return current(array_keys($array));
	}
}