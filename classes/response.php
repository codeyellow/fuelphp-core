<?php
namespace Core;

class Response extends \Fuel\Core\Response
{
    public static $statusesByName = null;

    public static function status($name)
    {
        static::$statusesByName === null && static::$statusesByName = array_flip(static::$statuses);
        
        return \Arr::get(static::$statusesByName, $name, null);
    }
}