<?php
namespace Core;

class Input extends \Fuel\Core\Input 
{
	/**
	 * Enable setting PHP input data.
	 *
	 * @return  void
	 */
	public static function setPutDelete($data)
	{
		// Force running hydration.
		static::put();
		static::$put_delete = $data;
	}
	
	
	/**
	 * Take into account different types of content type. Normally, Input::put() only parses put arguments
	 * using parse_str() while content body could be a json string.
	 * 
	 * @param string $method get|post|put|delete
	 * @param null|string $index
	 * @param null|string $default
	 */
	protected static function _rest($method, $index = null, $default = null)
	{
		$contentType = Input::headers('Content-Type') ?: Input::server('HTTP_CONTENT_TYPE');

		// Weird stuff with php-fpm + apache. Must test further.
        if (!$contentType && array_key_exists('CONTENT_TYPE', $_SERVER)) {
            $contentType = $_SERVER['CONTENT_TYPE'];
        }
		
		// Check request body for content.
		if(strtolower(Input::method()) == $method)
		{
			switch(true)
			{
				// Json.
				case stripos($contentType, 'application/json') !== false:
					$result = $index === null && $default === null ? static::json() : static::json($index, $default);
					break;

				// Xml.
				case stripos($contentType, 'text/xml') !== false:
				case stripos($contentType, 'application/xml') !== false:
					$result = $index === null && $default === null ? static::xml() : static::xml($index, $default);
					break;
			}
		}
		
		// On fail check normal methods.
		if((empty($result) || $result === $default))
		{
			$result = $index === null && $default === null ? parent::$method() : parent::$method($index, $default);
		}
		
		return $result;
	}
	
	public static function get($index = null, $default = null)
	{
		return static::_rest('get', $index, $default);
	}
	
	public static function post($index = null, $default = null)
	{
		return static::_rest('post', $index, $default);
	}
	
	public static function put($index = null, $default = null)
	{
		return static::_rest('put', $index, $default);
	}
	
	public static function delete($index = null, $default = null)
	{
		return static::_rest('delete', $index, $default);
	}

	public static function patch($index = null, $default = null)
	{
		return static::_rest('patch', $index, $default);
	}
	
	public static function raw_input()
	{
		return static::$xml . static::$php_input;
	}

	private static $headers = null;

	/**
	 * Fixes a bug where nginx and apache headers are parsed differently, make sure that
	 * all headers start with ucfirst.
	 */
	public static function headers($index = null, $default = null)
	{

		
		// Fix for apache
		if (static::$headers == null) {
			static::$headers = parent::headers();

			// nginx is actually done correctly in fuelphp
			// apache however is not
			if (function_exists('getallheaders')) {
				$newHeaders = array();
				foreach (static::$headers??[] as $key => $header) {
					$key = join('-', array_map('ucfirst', explode('_', strtolower($key))));
					$newHeaders[$key] = $header;
				}
				static::$headers = $newHeaders;
			}
		}

		return empty(static::$headers) ? $default : ((func_num_args() === 0) ? static::$headers : \Arr::get(static::$headers, $index, $default));
	}
 
 	/**
 	 * Overwrite the headers that were natively set. Only use if you know what you are doing.
 	 * @param $headers
 	 */
	public static function overwriteHeaders(array $headers) {
		static::$headers = $headers;
	}
}
