<?php
namespace Core;

class Date extends \Fuel\Core\Date
{
    /**
     * Get the week numbers in a certain month. The first week in 
     * a month must contain the first, while the last week is the
     * one before containing the first of the next month.
     *
     * @param int $year
     * @param int $month
     * @return  array [int, int, int ...]
     */
    public static function week_numbers_in_month($year, $month)
    {
        $first = \DateTime::createFromFormat('Y-m-d', "$year-$month-01");
        $last = \DateTime::createFromFormat('Y-m-d', "$year-" . ($month + 1) . "-01");

        return range($first->format('W'), $last->format('W') - 1);
    }

    /**
     * Get month of week.
     *
     * @param int $year
     * @param int $week
     * @return  array [year, month]
     */
    public static function week_to_month($year, $week)
    {
        for($month = 1; $month <= 12; $month++) {
            $weeks = static::week_numbers_in_month($year, $month);
            if (in_array($week, $weeks)) {
                return [$year, $month];
            }
        }
    }
}
