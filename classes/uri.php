<?php
namespace Core;

class Uri extends \Fuel\Core\Uri 
{
	/**
	 * Added $start paramater, modified uneven segments.
	 * 
	 * @param int $start
	 * @return array
	 */
	static function to_assoc($start = 0, $extension = false)
	{
		$segments = static::segments();
		$length = sizeof($segments);
		
		// Add extension.
		if($extension && $length > 0)
		{
			if(pathinfo($segments[$length - 1], PATHINFO_EXTENSION) !== \Input::extension())
			{
				$segments[$length - 1] .= '.' . \Input::extension();
			}
		}
		
		// Splice array.
		if($start > 0)
		{
			$segments = array_slice($segments, $start);
		}
		
		// Fix odd $segments, append an empty one.
		// Usually because of ../query/search_me => ../query/
		if(sizeof($segments) % 2 !== 0)
		{
			$segments[] = '';
		}
		
		return \Arr::to_assoc($segments);
	}
}