<?php
namespace Core;

class Validation extends \Fuel\Core\Validation
{
    /**
     * Checks whether input is string like.
     *
     * @param multi $val
     * @return bool
     */
    public static function _validation_string_like($val)
    {
        switch (true) {
            case is_null($val):
            case is_string($val):
            case is_numeric($val):
            case is_bool($val):
                return true;
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Checks whether numeric input is between a minimum and a maximum value
     *
     * @param   string|float|int
     * @param   float|int
     * @param   float|int
     * @return  bool
     */
    public static function _validation_unique($val, $options)
    {
        list ( $table, $field ) = explode('.', $options);
        
        $result = \DB::select("LOWER (\"$field\")")->where($field, '=', \Str::lower($val))->from($table)->execute();
        
        return !($result->count() > 0);
    }
}