<?php
namespace Core;

abstract class Session_Driver extends \Fuel\Core\Session_Driver {
    
    /**
     * Disable rotate. TODO: find out why session rotation kills app.
     * @return Ambigous <\Fuel\Core\Session_Driver, boolean, multitype:, NULL>
     */
    public function rotate($force = true)
	{
		return $this;
	}
}