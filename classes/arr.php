<?php
namespace Core;

class Arr extends \Fuel\Core\Arr
{

    public static function first($array)
    {
        return reset($array);
    }
    
    public static function last($array)
    {
        return end($array);
    }

    public static function firstKey($array)
    {
        return current(array_keys($array));
    }
    
    public static function lastKey($array)
    {
        $keys = array_keys($array);
        
        return end($keys);
    }

    public static function in_arrayi($needle, $haystack)
    {
        return in_arrayi($needle, $haystack);
    }

    /**
     * Implode associative array.
     * 
     * @param array $array
     * @param string $glue
     * @param string $glueKeyVal
     */
    public static function implodeAssoc($array, $glue = '', $glueKeyVal = '', $recursiveOpen = '', $recursiveClose = '') {
        $result = '';
        
        foreach($array as $k => $v) {
            $result .= $k . $glueKeyVal;

            if(is_array($v)){
                $result .= $recursiveOpen . static::implodeAssoc($v, $glue, $glueKeyVal, $recursiveOpen, $recursiveClose) . $recursiveClose;
            } else {
                $result .= $v;    
            }

            $result .= $glue;   
        }
        
        return substr($result, 0, -strlen($glue));
    }
    
    // Depricated.
    public static function implode_assoc($array, $glue = '', $glueKeyVal = '', $recursiveOpen = '', $recursiveClose = '') 
    {
        return static::implodeAssoc($array, $glue, $glueKeyVal, $recursiveOpen, $recursiveClose);
    }
    
    /**
     * Add prefix to array keys.
     * 
     * @param array $array
     * @param string $prefix
     * @return array
     */
    public static function prefixKeys(array $array, $prefix)
    {
        $result = array();

        foreach($array as $key => $val) {
            $result[$prefix . $key] = $val;
        }

        return $result;
    }

    // Depricated.
    public static function prefix_keys(array $array, $prefix)
    {
        return static::prefixKeys($array, $prefix);
    }

    /**
     * Extract only keys from $array.
     * 
     * @param array $array
     * @param array $keys
     * @return array
     */
    public static function extract(array $array, array $keys, $default = null) 
    {
        $result = array();

        foreach($keys as $key) {
            $result[$key] = static::get($array, $key, $default);
        }

        return $result;
    }

    /**
     * Original doesn't check key existance. 
     *
     * Pluck an array of values from an array.
     *
     * @param  array   $array  collection of arrays to pluck from
     * @param  string  $key    key of the value to pluck
     * @param  string  $index  optional return array index key, true for original index
     * @return array   array of plucked values
     */
    public static function pluck($array, $key, $index = null)
    {
        $return = array();
        $get_deep = strpos($key, '.') !== false;

        if ( ! $index)
        {
            foreach ($array as $i => $a)
            {
                //+Burhan: Added try block to prevent key undefined.
                try{
                    $return[] = (is_object($a) and ! ($a instanceof \ArrayAccess)) ? $a->{$key} :
                    ($get_deep ? static::get($a, $key) : $a[$key]);    
                } catch(\Exception $e) {
                    // Do nothing.
                }
                //-Burhan
                
            }
        }
        else
        {
            foreach ($array as $i => $a)
            {
                $index !== true and $i = (is_object($a) and ! ($a instanceof \ArrayAccess)) ? $a->{$index} : $a[$index];
                //+Burhan: Added try block to prevent key undefined.
                try{
                    $return[$i] = (is_object($a) and ! ($a instanceof \ArrayAccess)) ? $a->{$key} :
                        ($get_deep ? static::get($a, $key) : $a[$key]);
                } catch(\Exception $e) {
                    // Do nothing.
                }
                //-Burhan
            }
        }

        return $return;
    }
}