<?php
namespace Core;

class File extends \Fuel\Core\File
{
    private static $tempDirsToCleanup = [];

    /**
     * Create a temporary directory which will be automatically
     * cleaned up after the script terminates.
     */
    public static function createTempDir()
    {
        static $invocationCount = 0;
        $tmpdir = sys_get_temp_dir();
        $pid = getmypid();
        $time = microtime(true);

        $dirname = "$tmpdir/tempdir-$pid-$time-".($invocationCount++);
        // TODO: Attempt a loop and create some suffixes?  This _should_ be unique enough as is
        if (file_exists($dirname))
            throw new Exception("Error: tempdir `{$dirname}' already exists (this should not happen!)");
        if (!mkdir($dirname))
            throw new Exception("Error: tempdir `{$dirname}' could not be created (this should not happen!)");

        self::$tempDirsToCleanup[] = $dirname;

        return $dirname;
    }

    public static function cleanupTempDirs()
    {
        foreach(self::$tempDirsToCleanup as $dirname) {
            \File::delete_dir($dirname, true, true);
        }
        // Just in case one wants to manually call this, so that the
        // exit function won't try to clean them up again.
        self::$tempDirsToCleanup = [];
    }
}

// Register cleanup as soon as this class is loaded.

// TODO: It seems this is safe even when running inside a web server.
// The comments on the PHP manual vaguely imply it might not work?
if (function_exists('pcntl_signal')) {
    // This is to work around the situation that when a user presses
    // ^C, the shutdown functions aren't called.
    pcntl_signal(SIGINT, ['Core\File', 'cleanupTempDirs']);
    pcntl_signal(SIGTERM, ['Core\File', 'cleanupTempDirs']);
}

register_shutdown_function(['Core\File', 'cleanupTempDirs']);
