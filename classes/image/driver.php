<?php

/**
 * Part of the Fuel framework.
 *
 * Image manipulation class.
 *
 * @package		Fuel
 * @version		1.0
 * @license		MIT License
 * @copyright	2010 - 2011 Fuel Development Team
 * @link		http://fuelphp.com
 */

namespace Core;

abstract class Image_Driver extends \Fuel\Core\Image_Driver
{

	/**
	 * BUGFIX: Saving as differnt type reloads image with wrong extension. 
	 *
	 * @return Image_Driver
	 */
	public function reload()
	{
		$this->debug("Reloading was called!");
		$this->load($this->image_fullpath, false, false/*$this->image_extension*/);
		return $this;
	}
}

