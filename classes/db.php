<?php
namespace Core;

class DB extends \Fuel\Core\DB
{
    /**
     * Syntactic sugar for transactions. The supplied callback
     * is wrapped in a transaction if no active transaction
     * was found. 
     *
     * @param function $callback The callback to execute.
     */
    public static function trans($callback)
    {
        $trans = false;

        try {
            $trans = \DB::in_transaction();

            $trans || \DB::start_transaction();
            $result = $callback();
            $trans || \DB::commit_transaction();

            return $result;
        } catch (\Exception $e) {
            $trans || \DB::rollback_transaction();
            throw $e;
        }
    }
}
