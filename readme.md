# FuelPHP Core functionality extensions

# Installation

* Modify app/bootstrap.php from:

```
// Bootstrap the framework DO NOT edit this
require COREPATH.'bootstrap.php';
```

(pre 1.6) to:

```
// Bootstrap the framework DO NOT edit this
require COREPATH.'bootstrap.php';

/**
 * CY: Load Core package bootstrapper. Must be this early to
 * enable overwriting for example \Arr (it's used early in \Config load). 
 */
require APPPATH . 'vendor' . DS . 'codeyellow' . DS . 'fuelphp-core' . DS . 'bootstrap.php';
```

(post 1.6) to:

/**
 * CY: Load Core package bootstrapper. Must be this early to
 * enable overwriting for example \Arr (it's used early in \Config load). 
 */
require VENDORPATH . DS . 'codeyellow' . DS . 'fuelphp-core' . DS . 'bootstrap.php';
