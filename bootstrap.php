<?php
Autoloader::add_core_namespace('Core', true);

Autoloader::add_classes(array(
    'Core\\Input' => __DIR__ . DS . 'classes' . DS . 'input.php',
	'Core\\Uri' => __DIR__ . DS . 'classes' . DS . 'uri.php',
	'Core\\Arr' => __DIR__ . DS . 'classes' . DS . 'arr.php',
	'Core\\View' => __DIR__ . DS . 'classes' . DS . 'view.php',
	'Core\\App' => __DIR__ . DS . 'classes' . DS . 'app.php',
	'Core\\File' => __DIR__ . DS . 'classes' . DS . 'file.php',
	'Core\\Response' => __DIR__ . DS . 'classes' . DS . 'response.php',
	'Core\\Migrate' => __DIR__ . DS . 'classes' . DS . 'migrate.php',
	'Core\\Controller_Rest' => __DIR__ . DS . 'classes' . DS . 'controller' . DS . 'rest.php',
	'Core\\Format' => __DIR__ . DS . 'classes' . DS . 'format.php',
	'Core\\Debug' => __DIR__ . DS . 'classes' . DS . 'debug.php',
	'Core\\Security' => __DIR__ . DS . 'classes' . DS . 'security.php',
	'Core\\Session' => __DIR__ . DS . 'classes' . DS . 'session.php',
	'Core\\Session_Driver' => __DIR__ . DS . 'classes' . DS . 'session' . DS . 'driver.php',
	'Core\\Image_Driver' => __DIR__ . DS . 'classes' . DS . 'image' . DS . 'driver.php',
	'Core\\Validation' => __DIR__ . DS . 'classes' . DS . 'validation.php',
	'Core\\Date' => __DIR__ . DS . 'classes' . DS . 'date.php',
	'Core\\DB' => __DIR__ . DS . 'classes' . DS . 'db.php',
	'Core\\Database_Pdo_Connection' => __DIR__ . DS . 'classes' . DS . 'database' . DS . 'pdo' . DS . 'connection.php',
));

